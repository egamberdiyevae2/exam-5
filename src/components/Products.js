import React from "react";
import "./Products.css";

const Products = ({ products }) => {
  const handleAdd = (e) => console.log(e);
  return (
    <>
      {products.products.map((product) => (
        <div className="items">
          <h1 className="titles">{product.title}</h1>
          <div className="forimg">
            {product.images.map((img) => (
              <img className="images" src={img} alt="img" />
            ))}
          </div>
          <p>{product.description}</p>
          <strong>${product.price}</strong>
          <p>{product.brand}</p>

          <button className="btn" onClick={handleAdd(product.id)}>
            Add to cart
          </button>
        </div>
      ))}
    </>
  );
};

export default Products;
