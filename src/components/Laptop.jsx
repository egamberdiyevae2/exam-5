import React from "react";
import "./Laptop.css";

const Laptop = ({ laptops }) => {
  console.log(laptops);
  return (
    <div
      style={{
        gap: "40px",
        display: "flex",
        flexDirection: "column",
      }}
    >
      {laptops.map(
        (laptop) =>
          // console.log(laptop.category);
          laptop.category === "laptops" && (
            <div className="laptops">
              <div
                style={{
                  width: "500px",
                  overflow: "hidden",
                  display: "flex",
                }}
              >
                {laptop.images.map((img) => (
                  <img src={img} alt="img" />
                ))}
              </div>

              <h1>{laptop.title}</h1>
              <strong className="price">${laptop.price}</strong>
              <p>{laptop.description}</p>
              <h4 style={{ color: "green" }}>{laptop.rating}</h4>
              <button className="btn">Add to Cart</button>
            </div>
          )
      )}
    </div>
  );
};

export default Laptop;
