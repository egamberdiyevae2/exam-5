import React from "react";
import "./App.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Home from "./components/Home";
import Products from "./components/Products";
import Data from "./data/Data";
import Laptop from "./components/Laptop";
// import Nav from "./Layouts/Nav";

const App = () => {
  const products = Data;
  return (
    <div className="app">
      <div className="main">
        <a href="#3" style={{ textDecoration: "none" }}>
          <h1 className="title">Shopping Cart</h1>
        </a>

        <a href="#e" style={{ color: "black" }}>
          {" "}
          <i className="fa-solid fa-cart-shopping"></i>
        </a>
      </div>
      {/* <Link to="/home">Home</Link> */}
      <p>Manda Link ishlamadi</p>

      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/product" element={<Products products={products} />} />
          <Route
            path="/laptop"
            element={<Laptop laptops={products.products} />}
          />
        </Routes>
      </Router>
    </div>
  );
};

export default App;
